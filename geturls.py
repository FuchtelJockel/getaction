#!/usr/bin/env python3

import sys
import re
from bs4 import BeautifulSoup
import requests

def create_url(url):
    url = 'https://et.netaction.de/forum' + url[1:]
    cut = url.find('&sid=')
    if cut != -1:
        url = url[:cut - len(url)]
    return url

def parse_collection(collection):
    for topic in collection.find_all('a', {'class': 'forumtitle'}):
        url = create_url(topic['href'])
        print(topic.string, file=sys.stderr)
        get_topics(url)

def parse_topics(topics):
    for topic in topics.find_all('a', {'class': 'topictitle'}):
        url = create_url(topic['href'])
        print(url)

def get_topics(url):
    page = session.get(url);
    soup = BeautifulSoup(page.content, 'html.parser')
    collection_list = soup.find_all('div', {'class': re.compile('(forabg|forumbg)')})

    for collection in collection_list:
        collection_class = collection['class']
        collection_type = collection.find('div', {'class': 'list-inner'}).string

        if 'forabg' in collection_class and collection_type == 'Forum':
            parse_collection(collection)
            # ignore everything else
            break

        elif 'forumbg' in collection_class and collection_type == 'Themen':
            parse_topics(collection)


url = sys.argv[1]

# get credentials from cookie file
try:
    cookies_txt = open("cookies.txt")
except FileNotFoundError:
    print("cookies.txt nicht gefunden")
    exit(1)

jar = requests.cookies.RequestsCookieJar()
for line in cookies_txt:
    if line.startswith("#HttpOnly"):
        items = line.strip().split("\t")
        jar.set(items[5], items[6])

# get User-Agent
try:
    useragent_txt = open("useragent.txt")
except FileNotFoundError:
    print("useragent.txt nicht gefunden")
    exit(1)

useragent = next(useragent_txt).strip()

session = requests.Session()
session.headers.update({'user-agent': useragent})
session.cookies.update(jar)

get_topics(url)
