#!/usr/bin/bash

read -r -p "Nutzername: " username
read -r -s -p "Passwort: " password

useragent="Mozilla/5.0 (X11; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0"
printf "\nUser-Agent: %s\n" "$useragent"

login_page=$(curl -s -H "User-Agent: $useragent" 'https://et.netaction.de/forum/ucp.php?mode=login')

creation_time=$(echo "$login_page" | xmllint --html --xpath "string(//input[@name='creation_time']/@value)" - 2> /dev/null)
form_token=$(echo "$login_page" | xmllint --html --xpath "string(//input[@name='form_token']/@value)" - 2> /dev/null)
sid=$(echo "$login_page" | xmllint --html --xpath "string(//input[@name='sid']/@value)" - 2> /dev/null)

curl -i -X POST \
    --compressed \
    -c cookies.txt \
    -H "User-Agent: $useragent" \
    -b "phpbb3_netaction_u=1" \
    -b "phpbb3_netaction_k=" \
    -b "phpbb3_netaction_sid=${sid}" \
    -d "username=${username}" \
    -d "password=${password}" \
    -d "autologin=on" \
    -d "redirect=./ucp.php?mode=login" \
    -d "redirect=index.php" \
    -d "creation_time=${creation_time}" \
    -d "form_token=${form_token}" \
    -d "sid=${sid}" \
    -d "login=Anmelden" \
    'https://et.netaction.de/forum/ucp.php?mode=login'

echo "$useragent" > useragent.txt
