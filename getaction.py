#!/usr/bin/env python3

import sys
import os
from bs4 import BeautifulSoup
import requests

url = sys.argv[1]

def login(user, pw, useragent):
    headers = {'User-Agent': useragent}

    login_page = requests.get('https://et.netaction.de/forum/ucp.php?mode=login', headers=headers)
    cookies = login_page.cookies.get_dict()
    print(cookies)
    login_soup = BeautifulSoup(login_page.text, 'html.parser')

    token_elem = login_soup.find("input", {"type": "hidden", "name": "form_token"})
    token = token_elem['value']

    creation_time_elem = login_soup.find("input", {"type": "hidden", "name": "creation_time"})
    creation_time = creation_time_elem['value']

    sid_elem = login_soup.find("input", {"type": "hidden", "name": "sid"})
    sid = sid_elem['value']

    data = {
        "username": user,
        "password": pw,
        'autologin': 'on',
        "redirect": ["./ucp.php?mode=login", "index.php"],
        "creation_time": creation_time,
        "form_token": token,
        "sid": sid,
        "login": "Anmelden"
    }

    p = requests.post('https://et.netaction.de/forum/ucp.php?mode=login', data=data, headers=headers, cookies=cookies)

    print(p.status_code)
    print(p.request.body)
    print(p.cookies)

    return p.cookies.get_dict()


def parse_posts(soup, s):
    try:
        page_count = soup.find("div", {"class": "pagination"}).find_all("a", {"class": "button"})[-2].text
    except IndexError:
        page_count = 1
    print("Seiten:", page_count)
    pages = [url + '&start=' + str(n*25) for n in range(int(page_count))]
    posts = []
    for page in pages:
        r = s.get(page)
        soup = BeautifulSoup(r.content, 'html.parser')
        pst_soup = soup.find_all("div", {"class": "post"})
        fnames = {}
        for post in pst_soup:
            content = post.find("div", {"class": "content"})

            # find all attachments
            attachments = []
            attachbox = post.find("dl", {"class": "attachbox"})
            if attachbox:
                attachments = attachbox('dl')
            for div in content.find_all("div", {"class": "inline-attachment"}):
                attachments.append(div.dl)

            files = []
            for f in attachments:

                # falls f == None
                if not f:
                    continue

                name = ""
                link = ""
                des = ""
                if f['class'][0] == 'file':
                    try:
                        if f.dt['class'][0] == 'attach-image':
                            name = f.img['alt']
                            link = f.img['src']
                    except KeyError:
                        name = f.a.text
                        link = f.a['href']
                    try:
                        des = f.dd.em.decode_contents().strip()
                    except AttributeError:
                        des = ""
                elif f['class'][0] == 'thumbnail':
                    link = f.img.parent['href']
                    title = f.img['title'].split(' ')
                    name = ' '.join(title[:len(title)-5])
                    des = f.img['alt']
                    if des == name:
                        des = ""
                try:
                    fnames[name] += 1
                    fname, fext = os.path.splitext(name)
                    name = fname + '-' + str(fnames[name]) + fext
                except KeyError:
                    fnames[name] = 1
                files.append(
                    {
                        "name":  name.replace('/', '_'),
                        "link": link,
                        "des": des
                    }
                )

            # cleanup
            for img in content.find_all("img", {"class": "smilies"}):
                img.decompose()
            for div in content.find_all("div", {"class": "inline-attachment"}):
                div.decompose()
            for cite in content("cite"):
                cite.decompose()

            post = {
                "content": content.decode_contents(),
                "files": files
                }
            posts.append(post)
    return posts


# get credentials from cookie file
try:
    cookies_txt = open("cookies.txt")
except FileNotFoundError:
    print("cookies.txt nicht gefunden")
    exit(1)

jar = requests.cookies.RequestsCookieJar()
for line in cookies_txt:
    if line.startswith("#HttpOnly"):
        items = line.strip().split("\t")
        jar.set(items[5], items[6])

# get User-Agent
try:
    useragent_txt = open("useragent.txt")
except FileNotFoundError:
    print("useragent.txt nicht gefunden")
    exit(1)

useragent = next(useragent_txt).strip()

s = requests.Session()
s.headers.update({'user-agent': useragent})
s.cookies.update(jar)

thread = s.get(url)
soup = BeautifulSoup(thread.content, 'html.parser')

# check login
username = soup.find("li", {"id": "username_logged_in"})
if not username:
    print("Die Kekse sind kalt, Sitzung ist abgelaufen.")
    exit(1)
else:
    print("Nutzer:", username.a.span.text)

try:
    title = soup.find("h2", {"class": "topic-title"}).a.text
except AttributeError:
    # fixes f=149&t=12364
    if soup.find('h2', {'class': 'message-title'}).string == 'Information':
        print("Thread wurde gelöscht oder verschoben.")
    exit(1)

# validation
title = title.replace('/', '_')
title = title.replace(':', '_')

# get path
crumbs = soup.find("li", {"class": "breadcrumbs"})('a')
base_path = []
for i in range(1, len(crumbs)):
    base_path.append(crumbs[i].text)
base_path = os.path.join(*base_path).replace(":", "")
base_path = './netAction/' + base_path + '/' + title
print(base_path)

posts = parse_posts(soup, s)

os.makedirs(base_path, exist_ok=True)
os.chdir(base_path)

try:
    os.mkdir('./Anhänge')
except FileExistsError:
    ()

out_md = open('./' + title + '.md', 'w')
for (i, post) in enumerate(posts):
    out_md.write('## Beitrag ' + str(i) + ':\n')
    out_md.write(post['content'])
    out_md.write('\n')
    if not post['files'] == []:
        out_md.write('### Anhänge:\n')
        for f in post['files']:
            url = 'https://et.netaction.de/forum/' + f['link']
            name = f['name']
            path = './Anhänge/' + name
            if os.path.exists(path):
                print(name, "wird übersprungen")
            else:
                print('lädt:', name)
                dl = s.get(url)
                out = open(path, 'wb')
                out.write(dl.content)
            out_md.write('- ' + '[' + name + ']' + '(' + path + ')\n')
            if not f['des'] == "":
                out_md.write('    - ' + f['des'] + '\n')
    out_md.write('\n---\n')
