# Skript um Beiträge von netAction zu archivieren.

## Unterstützt:
- [x] Beiträge
- [x] Anhänge
- [x] Beiträge mit mehreren Seiten
- [x] Login mit Cookies
- [x] ganze Unterforen (mit Hilfe von `geturls.py`)
- [ ] Login mit Name & Passwort

## Abhängigkeiten installieren:
```
python -m pip install beautifulsoup4 requests
```
In Firefox das cookies.txt Addon installieren:
https://addons.mozilla.org/de/firefox/addon/cookies-txt/

## Vorbereitung:
1. `cookies.txt` in den Ordner mit dem getaction.py Skript legen.
2. `useragent.txt` erstellen
    In Firefox findet man den User-Agent unter `about:support`

## Nutzung:
Linux:
```sh
./getaction.py 'Thread URL'
```
Windows:
```sh
python.exe ./getaction.py 'Thread URL'
```

Mit `geturls.py` kann man ganze Ordner archivieren:
```sh
./geturls.py 'Ordner URL' | xargs -n1 ./getaction.py
```

Es ist zu empfehlen die URLs in einer Datei zu speichern.
```
./geturls.py 'Ordner URL' > links.txt
cat links.txt | xargs -n1 ./getaction.py
```

Die generierten Markdowndateien kann man mit einem Programm wie [discount](https://www.pell.portland.or.us/~orc/Code/discount/) zu HTML konvertieren.
```sh
discount-markdown -o index.html -html5 Dateiname.md
```
